package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author wzj
 * @email wzj@gmail.com
 * @date 2021-11-25 16:06:19
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
