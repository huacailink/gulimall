package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author wzj
 * @email wzj@gmail.com
 * @date 2021-11-25 16:27:38
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
