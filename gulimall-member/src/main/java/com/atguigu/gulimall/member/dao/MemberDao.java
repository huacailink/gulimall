package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author wzj
 * @email wzj@gmail.com
 * @date 2021-11-25 16:16:33
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
