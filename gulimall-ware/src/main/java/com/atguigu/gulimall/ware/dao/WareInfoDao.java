package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author wzj
 * @email wzj@gmail.com
 * @date 2021-11-25 16:34:08
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
