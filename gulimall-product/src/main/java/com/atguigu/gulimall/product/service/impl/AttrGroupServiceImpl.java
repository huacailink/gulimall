package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrGroupWithAttrsVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.AttrGroupDao;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.service.AttrGroupService;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {
    //负责属性的查询等功能
    @Autowired
    AttrService attrService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        //将不为0的模糊查询提取出来，以便于 在分类全部查询功能时也具有模糊查询。catelogId==0时也用模糊查询
        //就是查询的语句修改
        //构造查询条件
        String key = (String) params.get("key");
        //select * from pms_attr_group where catelog_id = ? and
        // (attr_group_id=key or attr_group_name like key)
        QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>();
        //
        if (!StringUtils.isEmpty(key)) {
            wrapper.and((obj) ->{
                obj.eq("attr_group_id", key).or().like("attr_group_name",key);
            });
        }
        //三级分类的id,id=0。就是在最上面
        if( catelogId ==0){
            //第一个参数是分页信息，第二个参数是根据分类id,选择。0则没有任何条件，返回所有
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params),
                    wrapper);
            //封装好的工具类，传入查询后的结果，工具类负责封装为分页信息
            return new PageUtils(page);
        }else {
            //cateId不为0的时候
            //params 中是否有查询的数据，key获取一下
            wrapper.eq("catelog_id", catelogId);
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params),
                    wrapper);
            return new PageUtils(page);
        }
    }

    /**
     * 根据分类id查出所有的分组以及这些组里面的属性
     * @param catelogId
     * @return
     */
    @Override
    public List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId) {
//        //这个根据id查出属性分组.查数据库，应该是pms_attr_group
//        //根据这个表中的catelogid字段来查出实体类集合，，因为不止一个、所哟应该就是这个service
//        //直接查
//        QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>();
//        wrapper.eq("catelog_id", catelogId);
//        //查询出分组
//        List<AttrGroupEntity> list = this.list(wrapper);
//        //查出属性
//        List<AttrGroupWithAttrsVo> collect = list.stream().map(attrGroupEntity -> {
//            //封装成vo
//            AttrGroupWithAttrsVo attrsVo = new AttrGroupWithAttrsVo();
//            BeanUtils.copyProperties(attrGroupEntity, attrsVo);
//            //vo数据还需要属性信息，根据id查出属性
//            List<AttrEntity> attrEntity = attrService.getRelationAttr(attrGroupEntity.getAttrGroupId());
//            attrsVo.setAttrs(attrEntity);
//            return attrsVo;
//        }).collect(Collectors.toList());
//
//
//        return collect;

        //com.atguigu.gulimall.product.vo
        //1、查询分组信息
        List<AttrGroupEntity> attrGroupEntities = this.list(new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId));

        //2、查询所有属性
        List<AttrGroupWithAttrsVo> collect = attrGroupEntities.stream().map(group -> {
            AttrGroupWithAttrsVo attrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(group,attrsVo);
            //获取attr关联的分组id
            //TODO:或许getgroupid为null
            List<AttrEntity> attrs = attrService.getRelationAttr(attrsVo.getAttrGroupId());
            attrsVo.setAttrs(attrs);
            return attrsVo;
        }).collect(Collectors.toList());

        return collect;

    }

}