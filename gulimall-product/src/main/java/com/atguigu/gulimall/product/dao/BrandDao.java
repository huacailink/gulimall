package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author wzj
 * @email wzj@gmail.com
 * @date 2021-11-25 13:59:58
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
