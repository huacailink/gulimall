package com.atguigu.gulimall.product.vo;

import lombok.Data;

//规格属性 的 响应数据，比attrVo多几个属性，继承一下
@Data
public class AttrRespVo extends AttrVo {
    /**
     * 			"catelogName": "手机/数码/手机", //所属分类名字
     * 			"groupName": "主体", //所属分组名字
     */
    private String catelogName;
    private String groupName;
    //p77还没有这个，应该是为了回显数据而写的，路径数组
    private Long[] catelogPath;
}
